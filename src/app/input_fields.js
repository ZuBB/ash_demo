// A hash with all possible input fields that will ever will be used
// in dialog(s) for this script
Script.inputFields = {
    'integer': {
        type: 'int'
    },
    'float': {
        type: 'float'
    },
    'string': {
        type: 'string'
    },
    'more': {
        type: 'combobox',
        value: ['inputs.combo.no', 'inputs.combo.yes']
    },
    '_42': {
        type: 'string',
        value: function() {
            return [
                Input.getValue('integer'),
                Input.getValue('float'),
                Input.getValue('string')
            ].join('; ');
        }
    }
};

