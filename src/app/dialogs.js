Script.dialogsContent = [
    'integer',
    'float',
    'string',
    'more',
    // adding next item to dialog is a tricky way to say that its enough
    // for this dialog to have inputs, lets push next items for new dialog.
    // after dialog is complete (in artificial way like above or natural
    // (7 inputs)) dialog window will be shown and wait for user's input
    Input.separator,

    function(){
        return Input.getValue('more') === 0 ? [] : ['_42'];
    }
];

