Dispatcher.registerNewTask({
    // [mandatory] name of the task
    // optionally may be used as a key to localize graphic name
    specName: '04_4196_graphics',
    // [optional] array of tasks names that must be executed before this one
    // if all task dependencies have been finished with successfull status
    // then this task can be executed, else (in case any of dependant tasks is
    // "failed") neither calc_data() nor make_props() function
    // will not be executed
    dependencies: ['01_4196_graphics'],

    make_props: function() {
        _d('hello');

        // Set view object description
        this.addProp4Views('example', 'description',
            [_t("views.example.description")]);

        // Add view object notation
        this.addProp4Views('example', 'notation',
                [_t("views.example.notation"), "info.html"]);
    }
});

