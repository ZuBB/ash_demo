Dispatcher.registerNewTask({
    specName: 'report_colors',

    calc_data: function() {
        _rp('');
        _rw('[ Red ]', {colors: [0xFFFFFF, 0xFF0000]});
        _rw('[ Purple ]', {colors: [0xFFFFFF, 0x800080]});
        _rw('[ Magenta ]', {colors: [0xFFFFFF, 0xFF00FF]});
        _rw('[ Green ]', {colors: [0xFFFFFF, 0x008000]});
        _rw('[ Lime ]', {colors: [0x000000, 0x00FF00]});
        _rw('[ Brown ]', {colors: [0xFFFFFF, 0x808000]});
        _rw('[ Yellow ]', {colors: [0x000000, 0xFFFF00]});
        _rw('[ Navy ]', {colors: [0xFFFFFF, 0x000080]});
        _rw('[ Blue ]', {colors: [0xFFFFFF, 0x0000FF]});
        _rw('[ Teal ]', {colors: [0xFFFFFF, 0x008080]});
        _rw('[ Aqua ]', {colors: [0x000000, 0x00FFFF]});

        _rw('', {colors: [0x000000, 0xFFFFFF]});
        _rp('');

        _rl('--------- Red Line ----------', {colors: [0xFFFFFF, 0xFF0000]});
        _rl('--------- Purple Line -------', {colors: [0xFFFFFF, 0x800080]});
        _rl('--------- Magenta Line ------', {colors: [0xFFFFFF, 0xFF00FF]});
        _rl('--------- Green Line --------', {colors: [0xFFFFFF, 0x008000]});
        _rl('--------- Lime Line ---------', {colors: [0x000000, 0x00FF00]});
        _rl('--------- Brown Line --------', {colors: [0xFFFFFF, 0x808000]});
        _rl('--------- Yellow Line -------', {colors: [0x000000, 0xFFFF00]});
        _rl('--------- Navy Line ---------', {colors: [0xFFFFFF, 0x000080]});
        _rl('--------- Blue Line ---------', {colors: [0xFFFFFF, 0x0000FF]});
        _rl('--------- Teal Line ---------', {colors: [0xFFFFFF, 0x008080]});
        _rl('--------- Aqua Line ---------', {colors: [0x000000, 0x00FFFF]});

        //DEBUG_START
        _i('hello world!')
        _d('these messages will be printed in log file')
        _w('there are several levels of log messages: debug, info, warn, error, fatal');
        _e('each of them can be invoked via next functions: _d, _i, _w, _e, _f');
        _f('everythin that is wrapped into DEBUG_{START,STOP} comments is stripped from release build');

        Dispatcher.addMessage('Erros you see at the bottom are intentional');
        Dispatcher.addMessage('See task `21_report_colors.js` for details');
        //DEBUG_STOP

        return true;
    }
});

