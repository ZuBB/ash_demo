Dispatcher.registerNewTask({
    specName: '05_4196_graphics',
    make_props: function() {
        var source = [
            '<html>',
            '  <head>',
            '    <title>Html report test</title>',
            '    <meta charset="UTF-8">',
            '  </head>',
            '  <body>',
            '    <h1>Hi!</h1>',
          //'    <input type="submit" value="Show graphic" onclick="Report.ActivateItem(1)" />',
            '    <input type="submit" value="Osc Pos test" onclick="Report.ShowOscPosition(1000020)" />',
            '  </body>',
            '</html>'
        ];

        CreateHTMLReport(source.join('\n'));

        // if calc_data() function appends some numbers to
        // internal data storage this automatically means that this task
        // approves its successfull status. in this case we do not append any
        // data, but we have everything OK here. so we need to sent some
        // kind of message to say 'we are OK'. calling 'return true;'
        // does exactly that. I guess we you already got what 'false' does...
        return true;
    }
});

