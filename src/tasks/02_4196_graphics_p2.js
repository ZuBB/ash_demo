Dispatcher.registerNewTask({
    specName: '02_4196_graphics',
    graphicName: 'area_graphics',

    viewIndex: 'example:3',
    // [optional] graphic color
    graphicColor: @AREA_COLORS@,
    // [optional] type of the graphics and line
    graphicType: 'area:thin',

    // Set vertical scale range
    // [optional] enables limits
    setLimits: true,
    // [optional] sets bottom limit. useless unless setLimits is on
    minLimit: 0,
    // [optional] sets top limit. useless unless setLimits is on
    maxLimit: 5.0,

    calc_data: function() {
        // Fill Area1GraphObj with points
        this.addDataSet({
            x: [5,   5,   15,   15],
            y: [0.0, 5.0,  5.0,  0.0]
        });

        // Fill Area2GraphObj with points
        this.addDataSet({
            x: [0,   10,   20],
            y: [0.0,  2.5,  0.0]
        });
    }
});

