Dispatcher.registerNewTask({
    specName: '03_4196_graphics',

    axisName: 'unit2',
    viewIndex: 'example:2',
    graphicType: 'multicolor',
    // [optional] graphicName may also be a function.
    // in that case you should take case about graphc name yourself
    graphicName: function(ii, length) {
        // _t() function is shortcut for function that does localization
        // its works in following way: you pass a key to get translated
        // string from hash that holds all of them and optionally
        // parameters for substitution
        return _t(['specs', 'multicolor_graphic', 'name'].join('.'));
    },

    setLimits: true,
    minLimit: 0,
    maxLimit: 5.0,

    calc_data: function() {
        // its also possible to add a data for graphic in this way
        this.addDataSet({
            x: [0, 5, 15, 20],
            y: [0, 2,  2,  0],
            color: [0xB00000, 0x008000, 0x0000B0, 0x000000]
        });
    }
});

