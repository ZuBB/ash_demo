Dispatcher.registerNewTask({
    // [mandatory] name of the task
    // optionally may be used as a key to localize graphic name
    // should be unique for each task else that task will be ignored
    specName: '01_4196_graphics',

    // [optional] graphics that will be produced by this task
    // will get index '1' in graphics array of view 'example'
    // (they will be drown first at that view)
    // [note] this is a key option that indicates ability to draw graphics
    // with data produces by this task
    viewIndex: 'example:1',
    // [optional] this task provides (approves creation) of graphics view
    // name of the view will be automatically localized
    // view will get index '1' in array of all provided views
    providedView: 'example:1',
    // [optional] key for that will be used to localize graphic unit
    axisName: 'unit1',
    // [optional] graphic color
    graphicColor: 0x000080,
    // [optional] type of line that will be used for drawing graphics
    graphicType: 2,

    // Set default (0) vertical scale step and color for graphics
    // [optional] enables scale
    setScale: true,
    // [optional] sets scale value. useless unless setScale is on
    scaleValue: 0,
    // [optional] sets scale color. useless unless setScale is on
    scaleColor: 0x00B000,

    // [optional] function that calculates data (for graphics in this case)
    calc_data: function() {
        for (var ii = 0; ii < 10; ii++) {
            this.addX(ii);
            this.addY(0.5 * ii);
        }

        for ( ; ii <= 20; ii++) {
            this.addX(ii);
            this.addY(10 - 0.5 * ii);
        }
    },
    // [optional] function that sets props for views
    // note: does not depend on task status
    make_props: function() {
        this.addProp4Views('example', 'scale', [1.0, 0xB0B0B0]);
    }
});

