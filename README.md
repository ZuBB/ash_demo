Directory structure
-------------------

Here is a directory structure of ash_demo project

```
.
├── build
└── src
    ├── app
    ├── core
    ├── resources
    └── tasks

```
After ASH framework is added, `src/core` directory contains next subdirs

```
.
├── build
└── src
    ├── core
    ├── extension
    ├── resources
    └── vendor
        ├── es5-shim
        │   └── tests
        │       ├── helpers
        │       ├── lib
        │       └── spec
        └── json

```

ASH framework can be added manually or via next git command since its also
included as another subrepository/(sub)module

```
git submodule update --init --recursive
```

After first build is run `build` directory is expanded into following structure

```
.
├── lib
├── output
└── tmp
    ├── i18n-reference
    └── i18n-storage
```

So, after all things are added directory structure comes to next state

```
.
├── build
│   ├── lib
│   ├── output
│   └── tmp
│       ├── i18n-reference
│       ├── i18n-storage
│       └── storage
└── src
    ├── app
    ├── core
    │   ├── build
    │   └── src
    │       ├── core
    │       ├── extension
    │       ├── resources
    │       └── vendor
    │           ├── es5-shim
    │           │   └── tests
    │           │       ├── helpers
    │           │       ├── lib
    │           │       └── spec
    │           └── json
    ├── resources
    ├── src
    └── tasks
```

* 'build' directory contains things that are related to build process. First that should be interesting for you is a 'output' dir. After build passed it contains results of the build.
* Everything that is under 'src/core' dir is a copy/clone of [ash](https://bitbucket.org/ZuBB/ash/overview) framework/library. As usual you should not change anything here unless you know what you are doing
* 'src/app' dir contains files that does toplevel stuff of our demo script
* 'src/resources' dir contains files with localized messages and colors. Each file has format of [Java properties file](http://en.wikipedia.org/wiki/.properties).
* 'src/tasks' dir contains javascript files with logical tasks. Successfull execution of tasks solves common goal we have

Software requirement
-------------------

* [Java SE Runtime Environment](http://www.oracle.com/technetwork/java/javase/downloads/jre7-downloads-1880261.html)
* [Apache Ant](http://ant.apache.org/bindownload.cgi)


Cooking a fresh build
-------------------

To get a fresh developer build use 'make_dev_build.bat' executable.
If you are ready for release use 'make_release_build.bat'.
You still want more power/flexibility check output of 'ant -p' within directory of demo script

